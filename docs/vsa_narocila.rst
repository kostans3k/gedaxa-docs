============
VSA NAROČILA
============

Naročilo ustvarimo, ko od našega partnerja sprejmemo naročilo za izdelavo (pol)izdelkov.

****************************
Kako ustvarim novo naročilo?
****************************

**Pogoji**: pred dodajanjem novega naročila, morate poskrbeti, da je stranka na katero se naročilo nanaša dodana
(za pomoč poglejte del dokumentacije z naslovom *Kako dodam novega partnerja, dobavitelja in/ali naročnika?*).
Šifrante, ki jih želite dodati kot postavke naročila, morate imeti vpisane (za pomoč poglejte del dokumentacije
z naslovom *Kako dodam nov šifrant?*). Prav tako morate imeti dodan projekt, znotraj katerega izvedete naročilo
(Za pomoč poglejte del dokumentacije z naslovom *Kako ustvarim nov projekt?*).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/list.png

Novo naročilo ustvarite tako, da izberete **Vsa naročila** (*Vodenje projektov > Vsa naročila*).
Pokaže se vam tabela s seznamom vseh naročil.
S klikom na gumb **Dodaj naročilo** se vam bo odprla forma za vnos podatkov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/dodaj.png

Vnos podatkov poteka v dveh korakih:
1. Ustvarimo novo naročilo
2. Dodamo postavke naročila

Ko ustvarimo novo naročilo, se odpre forma za vpis podatkov.
Obvezna polja so *Opis* naročila, *Status* naročila (odprto, potrjeno, preklicano), *Stranka* (vnesena kot šifrant partnerja)
in *Vrsta dobave* (osebno ali na naslov).
Po vnosu podatkov je obvezno klikniti na gumb shrani, da se podatki zabeležijo v sistem.

Podatki, ki smo jih vnesli, so shranjeni v zavihku *Glavno*, pred nami pa se pojavi zavihek *Postavke naročila*.
S klikom na gumb *Dodaj* se odpre forma za vnos podatkov o postavki.
Okno postavka črpa informacije iz baze šifrantov.

**********************************
Kako uredim naročilo ali postavke?
**********************************

Naročilo uredimo v podmeniju **Vsa naročila** (*Vodenje projektov > Vsa naročila*) s klikom na ikono za urejanje v
stolpcu Ukazi (|pencil|).
Urejamo lahko samo odprta naročila. Če naročilo ni odprto, glej poglavje *Kako odprem naročilo?*.

***********************************************************
Kaj naredim ko zaključim del naročila ali celotno naročilo?
***********************************************************

Ko zaključite del naročila ali celotno naročilo je za stranko potrebno izdelati dobavnico - glej del dokumentacije
pod *Vse dobavnice*.


.. |pencil| raw:: html

    <i class="icon-pencil2"></i>
