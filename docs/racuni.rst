======
RAČUNI
======

********************
Kako ustvarim račun?
********************

V glavnem meniju znotraj modula *Poslovanje* kliknemo na podmeni *Računi*. Odpre se tabela s seznamom vseh računov.
Pod tabelo kliknem na gumb *Ustvari račun*, ki odpre formo za vnos podatkov.
Iz padajoče vrstice izberem stranko. Stranka mora biti vnesena med naše partnerje, če še ni vnesena glej del dokumentacije
**Kako dodam novega partnerja, dobavitelja in/ali naročnika?**. V formo vnesemo *Rok plačila v dnevih*,
*Datum opravljene storitve* in *Datum računa*. Ko stisnemo *Potrdi* je račun ustvarjen in ga najdemo v *Seznamu računov*. Na
ustvarjeni račun moramo dodati postavke.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/racuni.png


*****************************
Kako dodam postavke na račun?
*****************************

Postavke dodajamo na že narejen račun. Če račun ni še ustvarjen, ga moramo ustvariti (glej **Kako ustvarim račun?**).
V glavnem meniju izberemo podmeni *Računi*, kjer se nam odpre seznam vseh računov. S klikom na številko računa (v prvem stolpcu)
se nam odpre pogled *Podrobnosti računa*. Obstajata dva načina dodajanja postavk na račun. S pritiskom na gumb *Dodaj postavko*
se odpre forma za vpis, kjer ročno vpišemo podrobnosti postavke. Lahko pa dodamo postavko s klikom na gumb
*Dodaj postavke iz dobavnice*, kjer zgolj izberemo dobavnico, ki smo jo izdelali za naročnika.
Prva možnost nam daje večjo fleksibilnost za določanje računov, če dobavnica ne vsebuje vseh informacij, ki bi jih dali na račun.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/racuni_detail.png

**********************
Kje najdem pdf računa?
**********************

V glavnem meniju izberemo podmeni *Računi*, kjer se nam odpre seznam vseh računov.
Miško postavimo na ikone v predzadnjem stolpcu (Ukazi), kjer najdemo ikono z naslovom PDF.
S klikom nanjo se generira račun v PDF obliki.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/racun_pdf.png

********************************
Kako potrdim, da je račun izdan?
********************************

V glavnem meniju izberemo podmeni *Računi*, kjer se nam odpre seznam vseh računov.
Miško postavimo na ikone v predzadnjem stolpcu (Ukazi), kjer najdemo ikono z naslovom *Označi kot izdano*.
S klikom nanjo se račun zabeleži kot izdani.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/racun_pdf.png

**Pozor:** ko enkrat potrdimo, da je račun izdan, tega ne moremo preklicati.

*********************************
Kako potrdim, da je račun plačan?
*********************************

V glavnem meniju izberemo podmeni *Računi*, kjer se nam odpre seznam vseh računov.
Miško postavimo na ikone v predzadnjem stolpcu (Ukazi), kjer najdemo ikono z naslovom *Označi kot plačano*.
S klikom nanjo se račun zabeleži kot izdani.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/racun_placan.png

**Pozor:** ko enkrat potrdimo, da je račun plačan, tega ne moremo preklicati.

*********************
Kako račun storniram?
*********************

V glavnem meniju izberemo podmeni *Računi*, kjer se nam odpre seznam vseh računov.
Miško postavimo na ikone v predzadnjem stolpcu (Ukazi), kjer najdemo ikono z naslovom *Prekliči račun*.
S klikom nanjo se račun zabeleži kot *Storniran račun*. Kateri računi so stornirani vidimo v stolpcu *Storniran račun*.

Stornacija računa nam ustvari nov račun z negativno vrednostjo prvotnega računa.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/racun_storniran.png

**Pozor:** ko enkrat potrdimo, da je račun storniran, tega ne moremo preklicati.

********************
Kako račun izbrišem?
********************

V glavnem meniju izberemo podmeni *Računi*, kjer se nam odpre seznam vseh računov.
Miško postavimo na ikone v predzadnjem stolpcu (Ukazi), kjer najdemo ikono z naslovom *Izbriši*.
S klikom nanjo se račun izbriše.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/racun_izbrisan.png

**Pozor:** ko enkrat izbrišetmo račun, tega ne moremo preklicati.

**Izbrišemo lahko samo zadnji račun.**
