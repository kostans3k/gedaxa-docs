==========
NASTAVITVE
==========

*******************
Nastavitve podjetja
*******************

Kako dodam podatke o podjetju na PDF dokumente?
+++++++++++++++++++++++++++++++++++++++++++++++

V glavnem meniju izberemo vrstico *Nastavitve*, ki odpre stran z različnimi nastavitvami.
Poleg napisa *Nastavitve podjetja* kliknemo gumb *Uredi*, ki odpre formo za vnos podatkov.
Podatke, ki vnesemo, shranimo z gumbom na dnu forme. Sedaj se prikažejo na PDF dokumentih.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/nastavitve_podjetja.png

Sistem avtomatično določi, kateri podatki se prikažejo na PDF-ju, glede na zahteve (npr. zahteve računa, dobavnice, itd.).
Lahko se pa odločite, katere informacije bi želeli dati v nogo (glej Kako dodam informacije o podjetju v nogo PDF dokumentov?).


Kako dodam logotip podjetja na PDF dokumente?
+++++++++++++++++++++++++++++++++++++++++++++

V glavnem meniju izberemo vrstico *Nastavitve*, ki odpre stran z različnimi nastavitvami.
Poleg napisa *Nastavitve podjetja* kliknemo gumb *Uredi*, ki odpre formo za vnos podatkov.
Na vrhu forme imamo zavihek za dodajanje datoteke z logotipom.
Shranimo z gumbom na dnu forme. Sedaj se logotip prikaže na PDF dokumentih.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/nastavitve_logo.png

*******************
Nastavitve tiskanja
*******************

Kako dodam informacije o podjetju v nogo PDF dokumentov?
++++++++++++++++++++++++++++++++++++++++++++++++++++++++

V glavnem meniju izberemo vrstico *Nastavitve*, ki odpre stran z različnimi nastavitvami.
Poleg napisa *Nastavitve tiskanja* kliknemo gumb *Uredi*, ki odpre formo za vnos podatkov.
Na desni vidimo značke, ki predstavljajo del besedila.
Preprosto kliknemo na tiste, ki bi jih želeli imeti v nogi dokumenta in se avtomatično prestavijo v okence Noga.
Če značke ločimo z vejicami, presledki, novimi vrsticami, itd. se bo tudi besedilo, ki ga značke predstavljajo,
tako ločil v nogi. Ko smo zadovoljni z izborom, kliknemo gumb *Shrani*. Sedaj se podatki prikažejo v nogi PDF dokumentov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/nastavitve_noga.png

*********************
Nastavitve poslovanja
*********************

Kako nastavim valuto, v kateri posluje podjetje?
++++++++++++++++++++++++++++++++++++++++++++++++

V glavnem meniju izberemo vrstico *Nastavitve*, ki odpre stran z različnimi nastavitvami.
Poleg napisa *Nastavitve poslovanja* kliknemo gumb *Uredi*.
Popelje nas na novo stran, kjer v okence napišemo željeno valuto in stisnemo *Shrani*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/nastavitve_valuta.png
