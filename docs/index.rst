.. gedaxa documentation master file, created by
   sphinx-quickstart on Wed Apr 26 11:23:43 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Gedaxa dokumentacija
====================


Dokumentacija je organizirana v sklope kot si sledijo v navigacijskem meniju Gedaxe:

* :ref:`glavni-meni`
* :ref:`kadrovanje`
* :ref:`vodenje-projektov`
* :ref:`skladiscenje`
* :ref:`proizvodnja`
* :ref:`poslovanje`
* :ref:`nastavitve`


.. _glavni-meni:

.. toctree::
   :maxdepth: 2
   :caption: Glavni meni

   dashboard
   moj_urnik
   poslovna_analitika


.. _kadrovanje:

.. toctree::
   :maxdepth: 2
   :caption: Kadrovanje

   zaposleni
   izmene
   potni_nalogi
   obracuni_plac
   organigram


.. _vodenje-projektov:

.. toctree::
   :maxdepth: 2
   :caption: Vodenje projektov

   seznam_projektov
   casovnice_projektov
   vse_ponudbe
   vsa_narocila
   vse_dobavnice


.. _skladiscenje:

.. toctree::
   :maxdepth: 2
   :caption: Skladiščenje

   material_in_skladisca
   inventura
   ostanki


.. _proizvodnja:

.. toctree::
   :maxdepth: 2
   :caption: Proizvodnja

   sifranti
   operacije
   procesi
   stroji
   delovni_nalogi


.. _poslovanje:

.. toctree::
   :maxdepth: 2
   :caption: Poslovanje

   partnerji
   dobava
   racuni


.. _nastavitve:

.. toctree::
   :maxdepth: 2
   :caption: Nastavitve

   nastavitve


=============================
Kako mi dokumentacija pomaga?
=============================

Dokumentacija razloži kako deluje vsak del Gedaxa ERP. Lahko ga beremo na dva načina:

1. Po sklopih glavnega menija (kot si sledijo funkcionalnosti v sistemu), ali
2. Poiščemo zgolj odgovor na konkretno vprašanje.

Zgrajen je bil s podjetji iz CNC industrije, za CNC industrijo, da odgovarja na potrebe industrije. Zato je logika
sistema, da deluje kot projekt. Če vnašamo nabavo, dobavo, naročila, dodelimo delavne naloge… vse je vezano na projekte,
ki so v osrčju delovanja Gedaxa ERP.


*******************************
Kako začnem uporabljati Gedaxo?
*******************************

Vsak sistem je zgrajen iz gradnikov. Isto velja za Gedaxa ERP. Da bi sistem pravilno deloval moramo vnesti nekatere
informacije, ki jih sistem zveže med sabo:

1. Podatke o zaposlenih
2. Izmene in delovne vloge v podjetju
3. Šifrante materialov in strojev
4. Operacije, cikle in procese strojev
5. Skladišča in zaloge materiala
6. Partnerje: dobavitelje in naročnike

Brez skrbi, dokumentacija in sam sistem vas vodi skozi vnašanje potrebnih informacij.
Proces vnašanja je pa narejen na uporabniku prijazen način.


*******************
Kaj pomenijo ikone?
*******************

Vseskozi Gedaxo uporabljamo iste ikone, da lažje (tj. z manj branja in pisanja) uporabljamo Gedaxo. Vedno uporabljamo
iste ikone s sledečimi pomeni:

.. raw:: html

    <table>
        <tr>
            <th>Ikona</th>
            <th>Opis</th>
            <th>Ikona</th>
            <th>Opis</th>
        </tr>
        <tr>
            <td><i class="icon-swap-vertical"></i></td>
            <td>Filtriraj podatke v tabeli naraščajoče, padajoče</td>
            <td><i class="icon-check"></i></td>
            <td>Potrdi oziroma zaključi</td>
        </tr>
        <tr>
            <td><i class="icon-arrow-1-right"></i></td>
            <td>Poglej podrobnosti podatkov v vrstici</td>
            <td><i class="icon-close"></i></td>
            <td>Izbriši podatek</td>
        </tr>
        <tr>
            <td><i class="fa icon-calendar"></i></td>
            <td>Datum</td>
            <td><i class="icon-cancel-circle"></i></td>
            <td>Prekliči tekoči projekt</td>
        </tr>
        <tr>
            <td><i class="icon-pencil2"></i></td>
            <td>Uredi podatek / vpis</td>
            <td></td>
            <td>Preglej in uredi skupino / člane skupine</td>
        </tr>
        <tr>
            <td><i class="icon-user-male-options"></i></td>
            <td>Uredi pravice skupine</td>
            <td><i class="icon-graph"></i></td>
            <td>Statistika</td>
        </tr>
        <tr>
            <td><i class="icon-file"></i></td>
            <td>PDF dokument</td>
            <td><i class="icon-files"></i></td>
            <td>Datoteke</td>
        </tr>
        <tr>
            <td><i class="fa icon-calendar"></td>
            <td>Časovno obdobje, trajanje</td>
            <td><i class="icon-bill-2"></i></td>
            <td>Ustvari obračun potnega naloga</td>
        </tr>
        <tr>
            <td><i class="icon-clipboard-checked"></i></td>
            <td>Izdan račun</td>
            <td><i class="icon-papyrus"></i></td>
            <td>Ustvari mesečno poročilo</td>
        </tr>
    </table>

Če miško postavite nad ikono, se prikaže opis ikone in njene funkcije, da v Gedaxi čim hitreje najdete, kar iščete.

Dokumentacija za uporabo Gedaxa sistema
=======================================


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>


=========
ZAPOSLENI
=========

Ta del dokumentacije se nanaša na sklop funkcionalnosti, ki jih najdete v podmeniju Zaposleni, ki se nahaja v modulu
Kadrovanje.

*****************
Seznam zaposlenih
*****************

Kako dodam novega zaposlenega?
++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se seznam vseh zaposlenih. S klikom na gumb Dodaj uporabnika se nam bo odprla forma za vnos podatkov.
Polja Uporabniško ime, E-mail, Delovno mesto in Geslo so obvezna. Delovna mesta so vnaprej določena,
če bi želeli drugo delovno mesto, nas prosim kontaktirajte. Vnos uporabnika zaključimo s klikom na gumb Potrdi.
Podatki o novem uporabniku se zapišejo v Seznam uporabnikov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dodaj-zaposlenega.png


Kako določimo za koliko ur je oseba zaposlena?
++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se seznam vseh zaposlenih. S klikom na opcijo Nastavitev tedenskih ur, za vsakega uporabnika ločeno,
se nam bo odprla forma za vnos podatkov. Vnos števila ur na teden zaključimo s klikom na gumb Potrdi.
Novi podatki o uporabniku so vidni v Seznamu uporabnikov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-tedenske-ure.png

Za polno zaposlitev uporabnika določimo 40 ur na teden.


Kako uredimo podatke o zaposlenih?
+++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se seznam vseh zaposlenih. S klikom na opcijo Uredi, za vsakega uporabnika ločeno, se nam bo odprla forma s
podatki uporabnika.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-urejanje.png

Podatke lahko spremenimo in spremembe shranimo s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-urejanje-potrdi.png


Kako odstranimo zaposlenega iz Seznama uporabnikov?
++++++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se nam seznam vseh zaposlenih. Pri vsakem uporabniku v tabeli se v stolpcu Ukazi nahaja ikona Izbriši.
S klikom na ikono, vas sistem vpraša, če ste prepričani, da želite odstraniti uporabnika. Izberite Potrdi.
S tem smo deaktivirali uporabnika. Ta se ne more več prijaviti v ERP s svojim uporabniškim imenom in geslom.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-izbrisi.png


Kje vidimo statistiko zaposlenih?
+++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se nam seznam vseh zaposlenih. Pri vsakem uporabniku v tabeli je v stolpcu Ukazi ikona Statistika.
S klikom na to ikono, se nam odpre statistika uporabnika.


******************
Skupine in pravice
******************

Kaj so skupine?
+++++++++++++++

Skupine so množice uporabnikov, ki uporabljajo Gedaxo na podoben način. Na primer delavec v proizvodnji bo v veliki
 meri pregledoval predvsem svoje delovne naloge in opravila za tekoči dan, skladiščnik bo pregledoval zaloge po
 skladiščih in pripravljal material glede na nivojsko sestavnico naročila, vodstvo pa bo pregledovalo metrike
 uspešnosti podjetja. Vsaki skupini lahko dodelimo zgolj določene pravice vpogledov in/ali urejanja informacij v ERP-ju.
 Pravice dodelimo glede na interne odloke o tem, kdo sme dostopati do informacij, in glede na to,
 katere informacije potrebuje zaposleni za opravljanje svojega dela.


Kako ustvarimo novo skupino?
++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Skupine in pravice.
Pokaže se tabela s seznamom vseh skupin. S klikom na gumb Dodaj skupino se nam bo odprla forma za vnos podatkov.
V formo vnesete samo ime skupine.
Vnos skupine zaključite s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ustvari-skupino.png


Kako dodamo ali odstranimo uporabnika iz skupine?
+++++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Skupine in pravice.
Pokaže se tabela s seznamom vseh skupin. Pri vsaki skupini v tabeli je v stolpcu Ukazi ikona Uredi člane.
Po kliku na to ikono se nam pokaže forma za urejanje članov skupine. Skupini dodamo uporabnika tako, da kliknemo na
ikono + pri željenem uporabniku. Ta se po kliku premakne v levi stolpec med dodane uporabnike tej skupini.
Skupini odstranimo uporabnika tako, da kliknemo na ikono X pri željenem uporabniku.
Ta se po kliku premakne v desni stolpec med ostale uporabnike, ki ne pripadajo tej skupini.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/skupina-urejaj-uporabnike.png


Kako definiramo pravice skupine?
++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Skupine in pravice.
Pokaže se tabela s seznamom vseh skupin. Pri vsaki skupini v tabeli je v stolpcu Ukazi ikona Uredi pravice.
Po kliku na to ikono se nam pokaže forma za urejanje pravic skupine.
Skupini dodamo pravico tako, da kliknemo na ikono + pri posamezni pravici. Ta se po kliku premakne v levi stolpec
med dodane pravice tej skupini. Skupini odstranimo pravice tako, da kliknemo na ikono X pri posamezni skupini.
Ta se po kliku premakne v desni stolpec med ostale pravice, ki jih ta skupina nima.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/skupina-urejaj-pravice.png


***********************
Planirana usposabljanja
***********************

Kako ustvarimo plan usposabljanja?
++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Planirana usposabljanja.
Tu lahko ustvarimo plane o usposabljanju zaposlenih, določimo za katero kvalifikacijo se zaposleni usposablja in
kako pogosto se usposabljanje ponavlja.
S klikom na gumb Dodaj usposabljanje se nam odpre forma za vnos podatkov o usposabljanju.
Vrsta Kvalifikacije, izbira Zaposlenega in Datum usposabljanja so obvezna polja. Če usposabljanje, ki ga ustvarjamo,
še nima vnesene kvalifikacije v sistem, lahko v formi kliknemo ikono + v oknu kvalifikacije in jo vpišemo.
V oknu Opis lahko dodamo poljubne informacije, ki bi bile koristne, npr. kontakt osebe, ki izvaja usposabljanje.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/plan-usposabljanja.png


Kako spremenimo oziroma osvežimo plane usposabljanja?
+++++++++++++++++++++++++++++++++++++++++++++++++++++

Če želimo urediti podatke za obstoječe usposabljanje, kliknemo na ikono Uredi. S klikom na ikono Označi kot končano
potrdimo, da je zaposleni opravil usposabljanje in sistem avtomatično določi nov datum usposabljanja glede na interval
ponovitve, ki smo ga določili.


Kje vidimo, kdo je potrdil ali spremenil plane usposabljanja?
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Planirana usposabljanja.
Kliknemo na gumb Dnevnik plana usposabljanja.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/plan-usposabljanja-dnevnik-klik.png

Odpre se nam stran, ki prikazuje, kdo je potrdil zaključek usposabljanja (Uporabnik) in kdaj je odobril zaključek
usposabljanja, oziroma označil, da je bilo usposabljanje opravljeno (Datum zaključka).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/usposabljanja-dnevnik.png

S klikom na konkretni primer usposabljanja znotraj stolpca Opis se odpre nov pogled Podrobnosti plana usposabljanja,
ki prikaže vse spremembe v usposabljanju za izbrano kvalifikacijo in danega zaposlenega.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/usposabljanje-podrobnosti.png


Kako usposabljanju dodamo kvalifikacijo?
++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Planirana usposabljanja.
Kliknemo na gumb Dnevnik plana usposabljanja. Na vrhu strani izberemo zavihek Kvalifikacije.
V tem pogledu lahko dodajamo imena kvalifikacij, za katere se usposabljajo zaposleni. Kvalifikacijo dodamo
s pritiskom na gumb Dodaj kvalifikacijo. Obstoječo kvalifikacijo uredimo s klikom na ikono Uredi.
Kvalifikacijo izbrišemo s klikom na ikono Izbriši.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/kvalifikacije.png


********************
Odsotnost zaposlenih
********************

Kako beležimo odsotnost zaposlenih?
+++++++++++++++++++++++++++++++++++

V podmeniju Zaposleni se nahaja opcija Odsotnost zaposlenih, kjer lahko beležimo, kdaj so bili zaposleni odsotni
od delovnega mesta. V polju Uporabniki izberete ime zaposlenega, za katerega želite vpisati odsotnost.
S klikom na Mesec izberete mesec, v katerem želite vpisati odsotnost. S klikom na gumb Potrdi, se osvežijo podatki
v spodnjih dveh tabelah.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/odsotnost-zaposlenih.png

Prva tabela Dodeljene ure za izbrani mesec prikaže v stolpcu Pričakovano število ur na teden za koliko ur je
delojemalec pogodbeno zaposlen in v stolpcu Število ur v izbranem mesecu koliko ur ima že dodeljenih na izmenah v
izbranem mesecu.

Pod tabelo je koledarski mesec razdeljen na tedne. Za vsak dan v tednu lahko s klikom označite, če je odsotnost
zaposlenega zaradi Bolniške, Dopusta ali Neplačanega dopusta.

Če želite izbrati daljše obdobje odsotnosti, lahko v oknu Koliko dni hkrati vnesete število dni odsotnosti
in pritisnete Potrdi. Sedaj v koledarju izberete prvi dan odsotnosti in sistem avtomatično zabeleži
odsotnost za število dni, ki ste jih izbrali.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/odsotnost-vec-dni.png

Možno je vpisati odsotnost za več zaposlenih istočasno. V polju Uporabniki izberete vse zaposlene, za katere
želite vpisati odsotnost. Po osvežiti koledarja se v prvem stolpcu pojavijo imena zaposlenih, za katere izberete
dneve odsotnosti. Odsotnost vsakega zaposlenega se beleži v svoji vrstici. Če ste izbrali opcijo Koliko dni hkrati se
vsakemu zaposlenemu avtomatično dodeli število odsotnih dni.
Z izbiro opcije Hkratna izbira omogočite, da se več zaposlenim zabeleži ista vrsta odsotnosti (npr. dopust) za
več dni (če ste izbrali to opcijo) z istim začetnim dnevom. Ta funkcionalnost poenostavi vpisovanje odsotnosti kot so
kolektivni dopusti ali turnusi izmen.

*********************
Prisotnost zaposlenih
*********************

Kako beležimo prisotnost zaposlenih?
++++++++++++++++++++++++++++++++++++

V podmeniju Zaposleni se nahaja opcija Prisotnost zaposlenih, kjer so zabeležene ure prisotnosti za posameznega
zaposlenega na določen dan. S klikom na gumb Dodaj prisotnost se odpre forma, kjer vnesemo ime zaposlenega iz
padajočega menija Zaposleni, Datum, na katerega se vnos nanaša, in Število opravljenih ur za izbran datum.
S klikom na gumb Potrdi se ure zabeležijo v tabeli Seznam prisotnosti.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dodaj-prisotnost.png

Zabeležene ure lahko uredite s klikom na ikono Uredi. Če želite pregledati prisotnost na določeni datum ali za
določenega zaposlenega (ali oboje) kliknete na Filtriraj tabelo in vpišete informacije po katerih želite filtrirati
Seznam prisotnosti.


=============
OBRAČUNI PLAČ
=============

================
SEZNAM PROJEKTOV
================

=============
VSE DOBAVNICE
=============

=====================
MATERIAL IN SKLADIŠČA
=====================

*Material in skladišča* nam ponuja pogled v katerem lahko urejamo material, ga preskladiščimo, pregledamo njegov
dnevnik, ter urejamo vsa naša skladišča.

********************
Kako uredim material
********************

Pred dodajanjem novega materiala, moramo poskrbeti, da se material nahaja v šifrantu (za pomoč poglejmo del
dokumentacije z naslovom *Kako dodamo nov šifrant*). Prav tako moramo imeti ustvarjeno skladišče v katerega želimo
dodati material (za pomoč poglejmo del dokumentacije z naslovom *Kako dodam novo skladišče?*).


Nov material dodamo tako, da v glavnem meniju izberemo *Material in skladišča* in nato v podmeniju možnost
*Uredi material*. Pokaže se nam tabela s seznamom vseh obstoječih materialov. Na dnu tabele izberimo možnost
*Uredi material*. S klikom na gumb *Uredi material* se nam odpre obrazec za vnos podatkov. Za *Dejanje* izberemo
eno izmed možnosti: *Dodaj na voljo* (Dodajanje materiala v skladišče), *Odstrani na voljo* (odstranjevanje materiala iz
skladišča), *Dodaj rezervacijo* (Dodajanje rezervacije na *Projekt*), *Odstrani rezervacijo* (odstranjevanje materiala
s *Projekta*, s tem tudi iz skladišča), *Spremeni rezeravcijo v na voljo* (odstranjevanje materiala s *Projekta* in
pri tem dodajanje v skladišče). S pomočjo iskalnega okna vnesemo material in skladišče.
Vnesemo še količino, ki jo želimo dodati, ter izbirno *Projekt* (v primerih, ko je ta potreben). Vnos materiala
zaključimo s klikom na gumb *Potrdi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_1.png


***************************************************
Kaj pomeni stolpec Na voljo/Pričakovano/Rezervirano
***************************************************

Stolpec *Na voljo/Pričakovano/Rezervirano* nam prikazuje razmerje med materialom, ki ga lahko uporabljamo, materialom,
ki ga pričakujemo (naročilo) in materialom, ki je rezerviran za projekte. Material, ki je rezerviran je vedno
podmnožica materiala, ki je na voljo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_reserved.png

****************************
Kako preskladiščimo material
****************************

Pogoj: preskladiščimo lahko samo material, ki je že v skladišču.

Če želimo material preskladiščiti iz trenutnega skladišča v skladišče, ki še ne obstaja, je potrebno novo skladišče
predhodno ustvariti (za pomoč poglejmo del dokumentacije z naslovom *Kako dodamo novo skladišče*).

Material lahko preskladiščimo na dva načina:

1. Preko grafičnega vmesnika
2. S pomočjo obrazca Uredi material

Preko grafičnega vmesnika preskladiščimo material tako, da v glavnem meniju izberemo *Material in skladišča*.
Pokaže se nam tabela s seznamom vseh materialov. Na dnu tabele s klikom na gumb *Preskladišči material* se nam odpre
obrazec za vnos podatkov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_transfer.png

Izbrati moramo material iz šifranta in količino, ki predstavlja eno enoto materiala, katerega želimo prenesti. S klikom
na gumb *Potrdi* se nam odpre grafični vmesnik za preskladiščenje materiala. Material lahko z akcijo povleci in spusti
(angl. drag&drop) poljubno premikamo med skladišči. Spremembe se shranijo samodejno.

Grafični vmesnik prikaže material za preskladiščenje v kockah, ki predstavljajo količino, ki smo jo določili v
obrazcu zgoraj. Kocke dajejo vpogled nad tem, kolikšen delež celotnega materiala želimo preskladiščiti. Zelene kocke
je možno preskladiščiti, medtem ko sivih ni, saj te predstaljajo material, ki jerezerviran za določen projekt.

S pomočjo obrazcca preskladiščimo material tako, da v glavnem meniju izberemo *Urejanje materiala* in nato v.
Pokaže se nam tabela s seznamom vseh materialov. Na dnu tabele s klikom na gumb *Uredi material* se nam odpre obrazec
za vnos podatkov. Za preskladiščenje moramo izbrati dejanje *Odstrani* (pri katerem odstranjujemo material iz skladišča)
in nato dejanje *Dodaj* (v katero skladišče želimo preskladiščiti).

Omejitve: preskladiščimo lahko samo en material hkrati. Grafični mesnik za preskladiščenje materiala ne moremo
uporabljati na zaslonih na dotik.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_transfer_gui.png

*********************************************************
Kako preverimo kaj se je s posameznim materialom dogajalo
*********************************************************

Kdo je dodal ali odstranil material, kdo ga je rezerviral, kam se je material premikal in drugo preverimo tako, da
v glavnem meniju izberemo *Material in skladišča*. Pokaže se nam tabela s seznamom vseh materialov. S klikom na gumb
*Poglej dnevnik* se nam odpre tabela, ki vsebuje podatke o tem kdo, koliko in kateri material se je iz posameznega
skladišča premikal ter kdaj se je to zgodilo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_log_1.png


.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_log_2.png


**************************
Kako dodamo novo skladišče
**************************

Novo skladišče dodamo tako, da v glavnem meniju izberemo *Material in skladišča* in nato v podmeniju možnost
*Uredi material*. Pokaže se nam tabela s seznamom vseh materialov. Na vrhu tabele izberimo možnost *Skladišče*.
S klikom na gumb *Dodaj skladišče* se nam odpre obrazec za vnos podatkov. *Vnesimo *Ime* (poljubno unikatno ime) in
opis. Urejanje zaključimo s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_warehouse_add.png

****************************************
Kaj je skladišče pričakovanega materiala
****************************************

Skladišče pričakovanega materiala je sistemsko skladišče, ki ga ni mogoče zbrisati. V njem se nahaja material, ki smo
ga naročili pri dobavitelju vendar zanj še nismo vnesli dobavnice. Z zaključevanjem dobavnice materiala se material
preskladiši v izbrana skladišča (zaključevanje dobavnice se nahaja v sekciji *Dobavnice*).
S klikom na gumb > poleg imena skladišča se nam prikaže zaloga materiala v skladišču.

*****************************
Kaj je Nerazvrščeno skladišče
*****************************

Nerazvrščeno skladišče je sistemsko skladišče, ki ga ni mogoče zbrisati. V njem se nahajajo  produkti, ki smo jih
naredili v naših procesih in jih skladiščimo po končanem proizvodnjem procesu. S klikom na gumb > poleg imena skladišča
se prikaže zaloga materiala v skladišču.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_warehouse_system.png

=========
INVENTURA
=========

=======
OSTANKI
=======

********************************************************************************************
Kam se shranijo ostanki materialov, ki nastanejo pri ciklih, in kako jih dodamo v skladišče?
********************************************************************************************

Vsi ostanki materialov, ki so nastali v primerih, ko smo ob kreiranju cikla nastavili ostanek, so prikazani v podmeniju
Ostanki, ki se nahaja v modulu Skladiščenje. Za vsak ostanek je na seznamu prikazana količina proizvedenega materiala,
ter ostanek (v procentih), ki smo ga definirali ob kreiranju cikla, hkrati pa na podlagi tega vidimo tudi preračunano
količino ostanka. Ostanki se v skladišče ne dodajo samodejo, pač pa lahko izberemo ostanke, ki naj se dodajo v
skladišče. Za dodajanje ostanka v skladišče za izbran ostanek kliknemo ikono Dodaj v skladišče. Odpre se nam obrazec,
v katerem se za količino izbere preračunana vrednost glede na definiran procent ostanka ter proizvedene količine.
Potrebno je izbrati skladišče, nato pa ostanek dodamo s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ostanki.png

V primeru, da želimo ostanek dodati v skladišče kot drug material ali spremeniti količino, ki se izračuna samodejno,
lahko to storimo v samem obrazcu - izberemo željeni material in količino.


========
ŠIFRANTI
========

Šifranti so edinstveni objekti v sistemu Gedaxa, ki so zapisani v obliki številk (šifer), ki predstavljajo vse
predmete, ki jih uporabljamo v naši proizvodnji, od materialov do izdelkov, od strojev do embalaže ter storitev.

Razdeljeni so ni tri dele (podmenije): *Material* (kamor spadajo izdelki, polizdelki, ter surov matrial),
*Ostalo* (orodja, stroji, kontrolna sredstva, pripomočki, pisarniška oprema, embalaža) ter *Storitve*. Za lažjo
prepoznavnost šifrantov, se ti na podlagi delitve začenjajo z različno številko (številke 1-11 predstavljene pred piko).
Številka za piko pa je samodejna naraščujoča, ki se poveča za vsak dodan nov izdelek (s tem sistem zagotavlja
unikatnost šifer). Poleg tega lahko šifrantu določimo tudi interno šifro (prvo število po piki) ter zunanjo šifro.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_1.png


***********************
Kako dodamo nov šifrant
***********************

V meniju *Šifranti* imamo vpogled do vseh naših šifrantov. Tu ji lahko tudi urejamo in dodajamo.

S pritiskom na gumb *Dodaj šifro* se odpre obrazec za vnos nove šifre. *Ime*, *Naziv 1* in *Naziv 2* so poljubna
poimenovanja za šifrant, ki ga želimo vnesti. Iz padajočega menija *Vrsta šifranta* izbiramo med možnostmi
*Izdelek*, *Polizdelek*, *Material*, *Orodja*, *Stroj*, *Kontrolna sredstva*, *Pripomočki*, *Pisarniška oprema*,
*Embalaža* in *Storitev*. Vsaka vrsta šifranta ima svojo pripadajočo številko, ki se prikaže v končni šifri kot prva
števka šifranta (številka pred piko). Posamezne vrste šifranta in njim pripadajoče številke so napisane na desni strani
pod tabelo.

Za lažjo prepoznavnost šifrantov določimo interno šifro, tj. eno števko od 0 do 9, ki bo v končni šifri dodana za piko.
Ostala števila so dodana avtomatično (s tem zagotovimo unikatnost vseh šifer). Seznam internih šifer si lahko
zabeležimo v prazno polje levo pod tabelo (in *Shranimo*), da nam je na razpolago pri prihodnjih vnosih šifrantov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_intern_numbers.png


V naslednjem koraku vpišemo koliko kilogramov, metrov, litrov in enot predstavlja 1 kos izbrnega šifranta. Ta
zapis razmerja med enotami je kasneje uporabljen pri preračunavanju uporabe šifrantov v procesih podjetja.  Količina
kilogram je obvezno polje, ki ga moramo izpolniti. Enote morajo biti celoštevilske saj le s tem sistem zagotavlja
pravilno računanje pretvarjanja enot. Za lažje račuanje enot (v primeru da nimamo celoštevilskih faktorjev) je na voljo
tudi računalo, ki po vnosu samodejno zapiše razmerja v obrazec.

Polje *Opis* je poljubno polje, v katerega lahko zapišemo opombo na šifrant. Po pritisku gumba *Shrani* se šifrant
zabeleži v tabeli *Seznam šifrantov*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_add_1.png

Ko je šifrant vnesen, ga lahko vedno *Uredimo* |pencil| ali *Zbrišemo* |delete| s pritiskom na
ikono v zadnjem stolpcu tabele *Seznam šifrantov*. Po *Tabeli šifrantov* lahko filtriramo po imenu ali (delni) šifri
šifrantov z uporabo funkcije *Filtriraj tabelo*, ki jo najdemo desno nad tabelo. Filtriranje je uporabno za iskanje
samo določene vrste šifrantov, npr. samo strojev, z vpisom iskalnega niza “5.”.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_filter.png

=========
OPERACIJE
=========

======
STROJI
======

***************
PREGLED STROJEV
***************

*****
CIKLI
*****

***********
VZDRŽEVANJE
***********

==============
DELOVNI NALOGI
==============

***********************
SEZNAM DELOVNIH NALOGOV
***********************

Kako ustvarim nov delovni nalog?
********************************
**Pogoj**: Da bi lahko ustvarili delovni nalog, moramo imeti vpisano naročilo naročnika. Če nimamo, glej *Kako ustvarim novo naročilo*?

Ko želimo ustvariti delovni nalog, odpremo zavihek v meniju Seznam delovnih nalogov (Glavni meni > Poslovanje > Delovni nalogi > Seznam delovnih nalogov) in kliknemo na gumb Dodaj delovni nalog. Odpre se forma za vnos, kjer izberemo naročilo, na katero se delovni nalog nanaša in potrdimo izbiro.

***************
MOJE ZADOLŽITVE
***************

*******************
CELOVIT POGLED DELA
*******************

======
DOBAVA
======

V dobavi najdemo vse kar je povezano z naročili dobave in dobavnicami. Vsa naročila in dobavnice so vezane na
dobavitelja, ter jih lahko začnemo ustvarjati šele ko imamo te definirane (gledamo sekcijo *Kako ustvarimo partnerja*)
Ko je naročilo ustvarjeno, se postavke na naročilu dodajo v skladišče pričakovanega materiala, ob zapiranju dobavnice pa
postavke in njihovo količino razporedimo v posamezna skladišča.

**************
SEZNAM NAROČIL
**************

V seznamu naročil vidimo vsa naročila, ki so bila ustvarjena v sistemu. Naročila imajo različna stanja (*Odprto*,
*Potrjeno* in *Preklicano*). Ko ustvarimo naročilo dobave ta dobi stanje *Odprto* in jo lahko urejamo vse dokler je ne
*Potridimo* oz. *Prekličemo*. Ko naročilo potrdimo se postavke in njihova količina zabeležita v *Skladišče pričakovanega
materiala* (gledamo sekcijo *Kaj je skladišče pričakovanega materiala*), ter ostane tam dokler ne zaključimo dobavnice,
ki se nanaša na to *Naročilo*

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement.png


Kako ustvarimo naročilo dobave
++++++++++++++++++++++++++++++

V *Seznamu naročil*, kliknemo na gumb *Dodaj naročilnico*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_1.png

Odpre se novo okno *Ustvari naročilnico*, kjer izpolnimo
manjkajoče podatke. Če dobavitelja ni na seznamu, ga moramo vnesti kot *Partnerja*, ali pa obstoječega partnerja
označiti kot dobavitelja (gledamo *Kako dodam novega partnerja, dobavitelja in/ali naročnika*).
Po kliku *Shrani*, se naročilo shrani, nato pa lahko začnemo dodajati postavke.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_2.png

Kako dodamo postavke in storitve na naročilo dobave
+++++++++++++++++++++++++++++++++++++++++++++++++++

V Seznamu naročil (*Dobava* > *Seznam naročil*), kliknemo na ikono za urejanje |pencil| v stolpcu *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_items_1.png

Odpre se okno *Posodobitve naročilnice*, kjer izberemo zavihek *Postavke naročila* (poleg zavihka *Glavno*), nato
pa pritisnemo gumb *Dodaj postavko*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_items_2.png

Odpre se obrazec za vnos podatkov, kjer iz padajočega menija izberemo postavko (če je ni, jo dodamo med šifrante,
glej *Kako dodamo nov šifrant*), količino postavke v izbrani merski enoti, cena na enoto in rok dostave postavke.

Pravilno shranjena postavka se prikaže v tabeli *Postavke naročila* na isti strani. Če želimo, jo lahko izbrišemo
ali uredimo s klikom na ikono v okolju *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_edit_items.png

Kako uredimo, izbrišemo, potrdimo, prekličemo naročilo ali nanj dodamo datoteke
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

V *Seznamu naročil* (*Dobava* > *Seznam naročil*) v stolpcu *Ukazi* najdemo v kolikor je naročilo odprto 5 ikon,
durgače 3 ikone, s katerimi lahko naročilo dobave uredimo, izpišemo, nanj dodamo datoteke, potrdimo ali ga prekličemo
ter izbrišemo. Urejanje, potrjevanje, preklicevanje naročila je možno v kolikor je njegovo stanje odprto.
Naročilo pa lahko izpišemo po tem ko smo ga potrdili.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_confirm.png

Zakaj ne morem dodati postavke na naročilo?
+++++++++++++++++++++++++++++++++++++++++++

Najverjetneje, ker je bilo naročilo potrjeno ali preklicano. V tem primeru moramo narediti novo naročilo dobave,
kamor dodajamo postavke.

*********
DOBAVNICE
*********

Dobavnice so vezane na naročila dobave. Ko je naročilo potrjeno se postavke in njihova količina zabeležijo v *Skladišču
pričakovanega materiala* (gledamo sekcijo *Kaj je skladišče pričakovanega materiala*), tam pa ostanejo dokler z
dobavnicami ne zaključimo celotne količine pričakovanega materiala (eno naročilo lahko zaključujemo z večimi
dobavnicami).

Kako ustvarim dobavnico
+++++++++++++++++++++++

V pogledu *Dobavnice* (*Dobava* > *Dobavnice*) kliknemo na gumb *Ustvari dobavnico*, ki odpre obrazec za vnos podatkov.
Šifro izberemo poljubno, dobavitelja pa izberemo iz padajočega menija. Če ne najdemo dobavitelja med možnostmi, moramo
dobavitelja vnesti (gledamo sekcijo *Kako dodam novega partnerja, dobavitelja in/ali naročnika*).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes.png

Ko imamo dobavnico ustvarjeno, lahko začnemo z zaključevanjem dobavljenih artiklov na posamezna naročila, ki so
bila poslana dobavitelju, katerega smo izbrali v prejšnem koraku. V pogledu *Seznam
dobavnic* v polju *Ukazi* kliknemo gumb za urejanje |pencil| (urejamo lahko vse dobavnice, ki še niso zaključene).
Ta nas pelje v novo okno, kjer so vidna vsa odprta naročila (naročila, ki še nimajo zaključene vseh količin postavk).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_edit.png

Dobavnica je razdeljena na vsa nezaključena naročila (naročila katere količine postavk še ni bila v celoti zaprta), ki
so bila poslana dobaviteljem. Za posamezno naročilo lahko zaključujemo vse postavke, vendar samo do količine, ki smo jo
definirali na samem naročilu. V kolikor je bil del postavke naročila zaključen že z drugo dobavnico (naročilo lahko
zaključujemo z večimi dobavnicami) nam sistem dovoli zaključiti le še preostanek količine, ki jo moramo še zaključiti.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_orders.png

Za posamezno postavko sistem prikazuje količino, ki smo jo definirali na naročilnico, količino koliko je bilo že zaprto
ter količino koliko moramo še zapreti. S pritiskom na gumb > se nam odpre obrazec za zaključevanje postavk naročila.
Izbrati moramo skladišče v katerega bomo posamezne postavke prenesli, sistem pa ob zaključevanju dobavnice samodejno
popravi zalogo teh artiklov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_to_close.png

Na posamezno dobavnico lahko dodajamo storitve, ki smo jih definirali v šifrantih (gledamo sekcijo *Kako dodajamo
šifrant*). Poleg tega lahko dobavnico poljubno shranjujemo, ter jo urejamo dokler je ne zaključimo.

Ko dobavnico v celoti izpolnemo jo lahko zaključimo s pritiskom na gumb *Zaključi*. Sistem samodejno doda artikle v
izbrana skladišča.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_finish.png

Ko je dobavnica zaključena jo lahko v pogledu *Seznam dobavnic* tudi izpišemo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_print.png

Kako dodamo datoteko na dobavnico
++++++++++++++++++++++++++++++++

V podmeniju *Vse dobavnice* (Dobava > Dobavnice), kliknemo na ikono za datoteke v vrstici dobavnice, na katero
želimo dodati datoteke. Odpre se novo okno Datoteke dobavnice.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_add_file_1.png


Datoteko dodamo v dveh korakih. Najprej kliknemo gumb *Dodaj datoteko*, nato pa v obrazcu za vnos naložimo dadoteko
iz računalnika, izberemo opis datoteke in stisnemo gumb *Shrani*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_add_file_2.png

