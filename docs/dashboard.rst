===============
NADZORNA PLOŠČA
===============

*********************************************
Kaj je nadzorna plošča in kako jo uporabljam?
*********************************************

Nadzorna plošča nam omogoča hiter pregled našega dela. Na njej najdemo štiri bližnjice:
*Opravila*, *Koledar*, *Moje zadolžitve na delovnih nalogih* in *Prisotnost*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dashboard.gif

V *Opravilih* najdemo tabelo s seznamom opravil, njihovim rokom ter osebo, ki je odgovorna za opravilo. Če želimo dodati
sebi ali drugim opravilo pritisnemo na gumb *Dodaj opravilo*, ter v obrazec vpišemo *Ime*, *Rok*, *Dodeljen zaposleni*
ter *Dodeljena skupina*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dashboard_tasks.png

Informacije v tabeli lahko sortiramo z gumbi zgoraj desno tako, da se nam prikažejo opravila, ki so *Odprta*
oziroma nedokončana, *Zaprta* oziroma končana/preklicana ali *Vsa opravila*. V zadnjem stolpcu *Ukazi* lahko opravilo
uredimo s pritiskom na ikono |pencil| ali ga označimo kot zaključenega s pritiskom na ikono |thick|. V primeru,
da se opravilo navezuje na objekt v sistemu Gedaxa, se lahko s klikom na |object| premaknjemo nanj.

*Koledar* združuje različne informacije v enotni pogled. Z izbiro časovne enote nad koledarjem *(dan, teden, mesec, seznam)*,
se prikažejo dogodki le za izbrano časovno obdobje. Če izberemo opcijo *Dnevni red*, nam koledar prikaže vrstični pogled
za vse dogodke v njihovem kronološkem vrstnem redu. V koledarju lahko z odkljukanjem izberemo, katere dogodke želimo
imeti prikazane: *Naročila, Opravila, Dobave, Vzdrževanje* in/ali uradne *Praznike*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dashboard_calendar.png

V oknu *Moje zadolžitve* vidimo, katera zadolžitev nam je bila dodeljena na določenem delovnem nalogu. Prikazane so samo
do danes nedokončane zadolžitve.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dashboard_my_tasks.png

Zavihek *Prisotnost* prikaže zadnjih sedem vnosov naše prisotnosti na delovnem mestu. Če želimo dodati novo prisotnost
pritisnemo na gumb *Dodaj današnjo prisotnost* in vpišemo število ur za izbrani datum. Prisotnost lahko dodajamo samo
zase. Prisotnosti ni mogoče dodajati za prihodnje ali pretekle dni. Če želimo dodati prisotnost za drugega zaposlenega
ali zase na dan iz preteklosti, moramo v glavnem meniju izbrati *Kadrovanje*, pogled *Urejanje zaposlenih* in zavihek
*Prisotnost zaposlenih*. Lahko pa preverimo prisotnost zaposlenih s pritiskom na gumb *Poglej prisotnost za vse zaposlene.*

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dashboard_logged_hours.png


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>
