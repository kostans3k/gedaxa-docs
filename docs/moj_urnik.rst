=========
MOJ URNIK
=========

Gumb Moj Urnik je prikladna bližnjica, ki vam pokaže samo vaš urnik po izmenah za izbrani mesec.
Urnik lahko pregledate za različne mesece z izborom Meseca in s klikom na gumb Potrdi, ki osveži stran.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/moj_urnik/urnik.png
